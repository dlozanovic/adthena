package com.dejanlozanovic.adthena


import java.io.{OutputStream, PrintStream}

import com.dejanlozanovic.adthena.inventory.Inventory
import com.dejanlozanovic.adthena.offer.{AppliedOffer, Discount, Offer}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalamock.scalatest.MockFactory
import org.scalatest.matchers.should.Matchers._

class PriceBasketTest extends AnyFlatSpec with MockFactory {

  "inventory" should "have Apples, Soup, Milk and Bread" in {
    PriceBasket.inventory.getOneProductByName("Apples").isRight shouldBe true
    PriceBasket.inventory.getOneProductByName("Soup").isRight shouldBe true
    PriceBasket.inventory.getOneProductByName("Milk").isRight shouldBe true
    PriceBasket.inventory.getOneProductByName("Bread").isRight shouldBe true
  }

  "offers" should "have offer that have discount on Apples" in {
    val apples = PriceBasket.inventory.getOneProductByName("Apples").right.get

    PriceBasket.offers.flatMap( offer => offer.applyOffer(Map(apples -> 1))) shouldBe List(AppliedOffer(List(Discount("Apples 10% off",0.1)),Map()))
  }

  it should "have offer Buy 2 soups to get bread 50% off" in {
    val soup = PriceBasket.inventory.getOneProductByName("Soup").right.get
    val bread = PriceBasket.inventory.getOneProductByName("Bread").right.get

    PriceBasket.offers.flatMap( offer => offer.applyOffer(Map(soup -> 2, bread -> 1))) shouldBe List(AppliedOffer(List(Discount("Buy 2 Soup and get Bread for 50% off",0.4)),Map()))
  }

  "run" should "call basket to process order and print it to out" in {
    val inventoryMock = mock[Inventory]
    val offers = List[Offer]()
    val basketMock = mock[Basket]
    val orderPrint = "42"

    val outputMock = mock[Output]

    (basketMock.processOrder _).expects(*,inventoryMock,offers,*,*).returning(orderPrint).once()
    (outputMock.printMessage _).expects(orderPrint).once()

    trait MockProxy extends Output {
      override def printMessage(message: String): Unit = outputMock.printMessage(message)
    }

    val priceBasket = new PriceBasket(inventoryMock,List(),basketMock) with MockProxy
    priceBasket.run(Array("Test"))

  }

  "main" should "call Pricebasket run method" in {
    val priceBasket = mock[PriceBasket]
    PriceBasket.priceBasket = priceBasket

    val args = Array("Apple")
    (priceBasket.run _).expects(args).once()

    PriceBasket.main(args)

  }




}
