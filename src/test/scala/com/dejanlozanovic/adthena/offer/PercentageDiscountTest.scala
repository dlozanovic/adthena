package com.dejanlozanovic.adthena.offer

import com.dejanlozanovic.adthena.Product
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class PercentageDiscountTest extends AnyFlatSpec {

  val apples = Product("Apples", 2.00)
  val oranges = Product("Oranges", 1.2)
  val percentageDiscount = new PercentageDiscount(apples, 10)

  "discountText" should "have product name and percentage " in {
    percentageDiscount.discountText shouldBe "Apples 10% off"
  }

  "applyOffer" should "return None if there are no apples in the basket" in {
    val basket = Map(oranges -> 2)

    percentageDiscount.applyOffer(basket) shouldBe None
  }

  it should "apply discount to all apples in the basket" in {
    val inputBasket = Map(oranges -> 2, apples -> 3)

    val expected = Some(AppliedOffer(List(Discount("Apples 10% off", 0.6)), Map(oranges -> 2)))

    percentageDiscount.applyOffer(inputBasket) shouldBe expected
  }

}
