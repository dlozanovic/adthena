package com.dejanlozanovic.adthena.offer

import com.dejanlozanovic.adthena.Product
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class MultiBuyDiscountTest extends AnyFlatSpec {
  val soup = Product("Soup", 0.65)
  val bread = Product("Bread", 0.80)
  val apples = Product("Apples", 1)

  "discountText" should "describe an offer" in {
    val offer = new MultiBuyDiscount(Map(soup -> 2, apples -> 4), bread, 50)

    offer.discountText shouldBe "Buy 2 Soup, 4 Apples and get Bread for 50% off"
  }

  "applyOffer" should "return None if there are no required and applied products in the basket" in {
    val offer = new MultiBuyDiscount(Map(soup -> 2), bread, 50)

    offer.applyOffer(Map(apples -> 2)) shouldBe None
    offer.applyOffer(Map(soup -> 2)) shouldBe None
    offer.applyOffer(Map(soup -> 1, bread -> 1)) shouldBe None
  }

  it should "apply discount on 4 soups and 1 bread" in {
    val offer = new MultiBuyDiscount(Map(soup -> 2), bread, 50)

    val expected = Some(AppliedOffer(List(Discount("Buy 2 Soup and get Bread for 50% off", 0.4)), Map(soup -> 2)))

    offer.applyOffer(Map(soup -> 4, bread -> 1)) shouldBe expected
  }

  it should "work with only one type of product too" in {
    val offer = new MultiBuyDiscount(Map(apples -> 1), apples, 50)

    offer.applyOffer(Map(apples -> 1)) shouldBe None

    offer.applyOffer(Map(apples -> 2)) shouldBe Some(AppliedOffer(List(Discount("Buy 1 Apples and get Apples for 50% off",0.5)),Map()))
  }
}
