package com.dejanlozanovic.adthena.offer

import com.dejanlozanovic.adthena.Product
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

// I know those test below should be mocked for isolation, but it was so tempting to make them like an integration tests
// because I wish to play with few more scenarios that are not covered in the requirement document :)

class OfferCalculatorTest extends AnyFlatSpec{
  val soup = Product("Soup", 0.65)
  val bread = Product("Bread", 0.80)
  val milk = Product("Milk", 1.30)
  val apples = Product("Apples", 1.00)

  val offerCalculator = new OfferCalculator()


  "applyOffers" should "return empty list if there are no valid offers" in {
    offerCalculator.findTheBestOffers(Map(soup -> 2, milk -> 1), List()) shouldBe List()

    val offers = List(
      new PercentageDiscount(apples, 10),
      new MultiBuyDiscount(Map(soup -> 2), bread, 50)
    )

    offerCalculator.findTheBestOffers(Map(soup -> 2, milk -> 1), offers) shouldBe List()

  }

  it should "apply apples 10% discount and buy 2 soup to get bread 50% off" in {
    val offers = List(
      new PercentageDiscount(apples, 10),
      new MultiBuyDiscount(Map(soup -> 2), bread, 50)
    )

    offerCalculator.findTheBestOffers(Map(soup -> 2, bread -> 2, apples -> 3), offers) shouldBe List(Discount("Buy 2 Soup and get Bread for 50% off", 0.4), Discount("Apples 10% off", 0.3))
  }

  it should "pick the highest discount" in {
    val offers = List(
      new PercentageDiscount(apples, 10),
      new MultiBuyDiscount(Map(apples -> 1), apples, 50)
    )

    offerCalculator.findTheBestOffers(Map(apples -> 2), offers) shouldBe List(Discount("Buy 1 Apples and get Apples for 50% off", 0.5))
    offerCalculator.findTheBestOffers(Map(apples -> 3), offers) shouldBe List(Discount("Buy 1 Apples and get Apples for 50% off", 0.5), Discount("Apples 10% off", 0.1))
    offerCalculator.findTheBestOffers(Map(apples -> 4), offers) shouldBe List(Discount("Buy 1 Apples and get Apples for 50% off", 0.5), Discount("Buy 1 Apples and get Apples for 50% off", 0.5))
  }


}
