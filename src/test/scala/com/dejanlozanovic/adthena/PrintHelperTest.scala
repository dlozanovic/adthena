package com.dejanlozanovic.adthena

import com.dejanlozanovic.adthena.offer.Discount
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class PrintHelperTest extends AnyFlatSpec {

  val soup = Product("Soup", 0.65)
  val bread = Product("Bread", 0.80)
  val milk = Product("Milk", 1.30)
  val apples = Product("Apples", 1.00)

  val printHelper = new PrintHelper

  "calculateSubTotal" should "calculate price for all items in basket" in {
    printHelper.calculateSubTotal(Map(apples -> 1, bread -> 5)) shouldBe 5
    printHelper.calculateSubTotal(Map(milk -> 1, bread -> 5)) shouldBe 5.30
  }

  "calculateDiscountTotal" should "calculate total discount" in {
    printHelper.calculateDiscountTotal(List()) shouldBe 0
    printHelper.calculateDiscountTotal(List(Discount("foo", 0.65), Discount("bar", 0.30))) shouldBe 0.95
  }

  "currency" should "convert number into a currency string" in {
    printHelper.currency(1) shouldBe "£1.00"
    printHelper.currency(1.347) shouldBe "£1.35"
    printHelper.currency(0.33) shouldBe "£0.33"
  }

  "discountToString" should "convert all discounts into a string" in {
    printHelper.discountToString(List()) shouldBe "(No offers available)"
    printHelper.discountToString(List(Discount("foo", 0.65), Discount("bar", 0.30))) shouldBe "foo: £0.65\nbar: £0.30"
  }

}
