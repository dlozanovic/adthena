package com.dejanlozanovic.adthena.inventory

import com.dejanlozanovic.adthena.Product
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class MemoryInventoryTest  extends AnyFlatSpec{

  "getOneProductByName" should "return product if it exists " in {
    val product = Product("Apple", 1.0)
    val inventory = new MemoryInventory(List(product))

    inventory.getOneProductByName(product.name) shouldBe Right(product)
  }

  it should "return error message if inventory doesn't have product" in {
    val product = Product("Apple", 1.0)
    val inventory = new MemoryInventory(List(product))

    inventory.getOneProductByName("Orange").isLeft shouldBe true
  }

  "getAllProductsByName" should "return list of product if they exists " in {
    val products = List(Product("Apple", 1.0),Product("Apple", 1.34))
    val inventory = new MemoryInventory(products)

    inventory.getAllProductsByName("Apple").right.get.toSet shouldBe products.toSet
  }

  it should "return error message if inventory doesn't have any product with given name" in {
    val products = List(Product("Apple", 1.0),Product("Apple", 1.34))
    val inventory = new MemoryInventory(products)

    inventory.getAllProductsByName("Orange").isLeft shouldBe true
  }

//  "MemoryInventory" should "contain Soup, Bread, Milk and Apples" in {
//    val inventory = MemoryInventory()
//
//    inventory.getOneProductByName("Soup").isRight shouldBe true
//    inventory.getOneProductByName("Bread").isRight shouldBe true
//    inventory.getOneProductByName("Milk").isRight shouldBe true
//    inventory.getOneProductByName("Apples").isRight shouldBe true
//  }


}
