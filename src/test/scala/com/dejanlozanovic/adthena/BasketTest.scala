package com.dejanlozanovic.adthena

import com.dejanlozanovic.adthena.inventory.Inventory
import com.dejanlozanovic.adthena.offer.{Discount, Offer, OfferCalculator}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalamock.scalatest.MockFactory
import org.scalatest.matchers.should.Matchers._

class BasketTest extends AnyFlatSpec with MockFactory {
  val soup = Product("Soup", 0.65)
  val bread = Product("Bread", 0.80)
  val milk = Product("Milk", 1.30)
  val apples = Product("Apples", 1.00)

  val basket = new Basket()

  "createBasket" should "return List of products that It can't retrieve from inventory" in {
    val inventoryMock = mock[Inventory]

    (inventoryMock.getOneProductByName _).expects("Orange").returning(Left("foo")).once()

    basket.createBasket(inventoryMock, List("Orange")) shouldBe Left(List("foo"))
  }

  it should "return basket of Product and quantities " in {
    val inventoryMock = mock[Inventory]

    (inventoryMock.getOneProductByName _).expects("Apples").returning(Right(apples)).once()
    (inventoryMock.getOneProductByName _).expects("Milk").returning(Right(milk)).once()

    basket.createBasket(inventoryMock, List("Milk", "Apples", "Apples")) shouldBe Right(Map(apples -> 2, milk -> 1))
  }

  "processOrder" should "create output from input strings " in {
    val inventoryMock = mock[Inventory]
    (inventoryMock.getOneProductByName _).expects("Apples").returning(Right(apples)).once()
    (inventoryMock.getOneProductByName _).expects("Milk").returning(Right(milk)).once()

    val offers = List[Offer]()

    val offerCalculatorMock = mock[OfferCalculator]
    val discounts = List(Discount("foo",42))

    (offerCalculatorMock.findTheBestOffers _).expects(*,*).returning(discounts)

    val printHelperMock = mock[PrintHelper]
    val subtotal:BigDecimal = 63
    val totalPrice:BigDecimal = 42

    (printHelperMock.calculateSubTotal _).expects(*).returning(subtotal)
    (printHelperMock.currency _).expects(subtotal).returning("£63.00")
    (printHelperMock.currency _).expects(totalPrice).returning("£42.00")
    (printHelperMock.calculateTotalPrice _).expects(*,*).returning(totalPrice)
    (printHelperMock.discountToString _).expects(discounts).returning("foo: 21")

    val expected =
      """Subtotal: £63.00
        |foo: 21
        |Total price: £42.00
        |""".stripMargin

    basket.processOrder(List("Milk", "Apples", "Apples"),inventoryMock,offers,offerCalculatorMock,printHelperMock) shouldBe expected
  }

}
