package com.dejanlozanovic.adthena.inventory

import com.dejanlozanovic.adthena.Product

trait Inventory {
  def getOneProductByName(name: String): Either[String, Product]

  def getAllProductsByName(name: String): Either[String, List[Product]]
}
