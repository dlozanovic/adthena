package com.dejanlozanovic.adthena.inventory

import com.dejanlozanovic.adthena.Product

class MemoryInventory(products:List[Product]) extends Inventory {

  private val inventory = products.groupBy(_.name)

  override def getOneProductByName(name: String): Either[String, Product] = {
    inventory.get(name).map(_.head).toRight(s"Missing product: ${name}")
  }

  override def getAllProductsByName(name: String): Either[String, List[Product]] = {
    inventory.get(name).toRight(s"Missing product: ${name}")
  }

  def allProducts:List[Product] = products

}
