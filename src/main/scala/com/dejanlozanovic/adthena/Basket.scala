package com.dejanlozanovic.adthena

import com.dejanlozanovic.adthena.inventory.Inventory
import com.dejanlozanovic.adthena.offer.{Offer, OfferCalculator}

class Basket {

  def createBasket(inventory: Inventory, productNames: List[String]): Either[List[String], Map[Product, Int]] = {
    val products = productNames.groupBy(product => product).map(kv => (inventory.getOneProductByName(kv._1), kv._2.length))

    val missingProducts = products.filter(_._1.isLeft).map(_._1.left.get)

    if (missingProducts.nonEmpty) {
      Left(missingProducts.toList)
    } else {
      Right(products.map(kv => (kv._1.right.get, kv._2)))
    }
  }

  def processOrder(productNames: List[String], inventory: Inventory, offers: List[Offer], offerCalculator: OfferCalculator, ph: PrintHelper): String = {
    createBasket(inventory, productNames) match {
      case Left(missingProducts) => missingProducts.mkString("Can't calculate basket\n", "\n", "")
      case Right(basket) =>
        val subTotal = ph.currency(ph.calculateSubTotal(basket))
        val discounts = offerCalculator.findTheBestOffers(basket, offers)
        val totalPrice = ph.currency(ph.calculateTotalPrice(basket,discounts))

        val allDiscounts = ph.discountToString(discounts)

        s"""Subtotal: $subTotal
           |$allDiscounts
           |Total price: $totalPrice
           |""".stripMargin
    }
  }
}
