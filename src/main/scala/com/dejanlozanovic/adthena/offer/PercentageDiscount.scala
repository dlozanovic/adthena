package com.dejanlozanovic.adthena.offer

import com.dejanlozanovic.adthena.{Product, offer}

class PercentageDiscount(product:Product,percentage:BigDecimal) extends Offer {

  private val discountPerItem = product.price * percentage / 100

  override def applyOffer(basket: Map[Product, Int]): Option[AppliedOffer] = {
    basket.get(product).map { amount =>
      offer.AppliedOffer(List(Discount(discountText,amount*discountPerItem)),basket - product)
    }
  }

  def discountText:String = s"${product.name} $percentage% off"

}
