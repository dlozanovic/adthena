package com.dejanlozanovic.adthena.offer

import com.dejanlozanovic.adthena.{Product, offer}

class MultiBuyDiscount(requiredProducts:Map[Product, Int], appliedProduct:Product, percentage:BigDecimal) extends Offer {

  private val allProducts = requiredProducts.get(appliedProduct) match {
    case Some(amount) => requiredProducts + (appliedProduct -> (amount + 1) )
    case None => requiredProducts + (appliedProduct -> 1)
  }

  private val discount = List(Discount(discountText, appliedProduct.price * percentage / 100))

  override def applyOffer(basket: Map[Product, Int]): Option[AppliedOffer] = {

    val productsToUpdate = for( (product ,requiredAmount) <- allProducts) yield basket.get(product) match {
      case Some(amount) => if(amount>=requiredAmount) Some(product, amount - requiredAmount) else None
      case None => None
    }

    if(productsToUpdate.filter(_.isEmpty).nonEmpty) {
      None
    } else {
      val updatedProducts = (basket ++ productsToUpdate.flatten).filter(_._2 > 0)
      Some(offer.AppliedOffer(discount, updatedProducts))
    }
  }

  def discountText:String = {
    val required = requiredProducts.map( kv => s"${kv._2} ${kv._1.name}").mkString(", ")
    s"Buy $required and get ${appliedProduct.name} for $percentage% off"
  }
}
