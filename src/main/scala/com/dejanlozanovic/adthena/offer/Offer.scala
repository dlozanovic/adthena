package com.dejanlozanovic.adthena.offer

import com.dejanlozanovic.adthena.Product

case class Discount(name:String,discount:BigDecimal)
case class AppliedOffer(discounts:List[Discount], unaffectedProducts:Map[Product,Int])

trait Offer {
  def applyOffer(basket:Map[Product,Int]) : Option[AppliedOffer]
}
