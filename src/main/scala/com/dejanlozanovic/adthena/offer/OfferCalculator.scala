package com.dejanlozanovic.adthena.offer

import com.dejanlozanovic.adthena.Product

import scala.collection.mutable

class OfferCalculator {

  def findTheBestOffers(basket: Map[Product, Int], offers: List[Offer]): List[Discount] = {
    if (offers.isEmpty) return List()

    // dynamic programing optimization
    val cached = mutable.Map.empty[Map[Product, Int], List[Discount]]


    def findOffers(basket: Map[Product, Int], offers: List[Offer]): List[Discount] = {
      cached.get(basket) match {
        case Some(discounts) => discounts
        case None =>
          val appliedOffers = offers.map { offer =>
            offer.applyOffer(basket) match {
              case Some(appliedOffer) => appliedOffer.discounts ::: findTheBestOffers(appliedOffer.unaffectedProducts, offers)
              case None => List()
            }
          }

          val result = appliedOffers.sortBy(_.map(_.discount).sum).last
          cached += (basket -> result)
          result
      }
    }

    findOffers(basket, offers)
  }

}
