package com.dejanlozanovic.adthena

import java.io.PrintStream

import com.dejanlozanovic.adthena.inventory.{Inventory, MemoryInventory}
import com.dejanlozanovic.adthena.offer.{MultiBuyDiscount, Offer, OfferCalculator, PercentageDiscount}

trait Output {
  def printMessage(message:String) = Console.println(message)
}


class PriceBasket(inventory: Inventory, offers:List[Offer], basket: Basket) extends Output {
  val offerCalculator = new OfferCalculator
  val printHelper = new PrintHelper

  def run(args: Array[String]):Unit = {
    val output = basket.processOrder(args.toList,inventory,offers,offerCalculator,printHelper)
    printMessage(output)
  }
}


object PriceBasket {

  def activeOffers(apples: Product, soup: Product, bread: Product): List[Offer] = {
    List(
      new PercentageDiscount(apples, 10),
      new MultiBuyDiscount(Map(soup -> 2), bread, 50)
    )
  }

  def createDependencies() = {
    val soup = Product("Soup", 0.65)
    val bread = Product("Bread", 0.80)
    val milk = Product("Milk", 1.30)
    val apples = Product("Apples", 1.00)
    (new MemoryInventory(List(soup,bread,milk,apples)),activeOffers(apples,soup,bread))
  }

  val (inventory,offers) = createDependencies()
  var priceBasket = new PriceBasket(inventory, offers,new Basket)

  def main(args: Array[String]): Unit = {
    priceBasket.run(args)
  }
}
