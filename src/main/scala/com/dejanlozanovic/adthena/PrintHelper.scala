package com.dejanlozanovic.adthena

import java.text.NumberFormat
import java.util.Locale

import com.dejanlozanovic.adthena.offer.Discount

class PrintHelper {
  private val numberFormat = NumberFormat.getCurrencyInstance(new Locale("en", "GB"))

  def calculateSubTotal(basket: Map[Product, Int]): BigDecimal = basket.map(kv => kv._1.price * kv._2).sum

  def calculateDiscountTotal(discounts: List[Discount]): BigDecimal = discounts.map(_.discount).sum

  def currency(amount: BigDecimal): String = numberFormat.format(amount)

  def calculateTotalPrice(basket: Map[Product, Int], discounts: List[Discount]):BigDecimal = calculateSubTotal(basket) - calculateDiscountTotal(discounts)

  def discountToString(discounts: List[Discount]) = {
    if (discounts.isEmpty) {
      "(No offers available)"
    } else {
      discounts.map(discount => s"${discount.name}: ${currency(discount.discount)}").mkString("\n")
    }
  }
}
