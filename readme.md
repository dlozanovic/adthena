# Installation

## Requirements
java installed on host computer and in path  
sbt installed on host computer and in path  

## Procedure how to build the program
Run the folloving command:  
sbt clean assembly

# Running the program

On Windows:  
java -jar target\scala-2.12\adthena-assembly-0.1.jar **(list of products)**  

On Linux & mac os:  
java -jar target/scala-2.12/adthena-assembly-0.1.jar **(list of products)**  

# Supported products
* Apples
* Soup
* Bread
* Milk
